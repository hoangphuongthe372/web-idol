import React, { Component } from 'react';
import './Product.css'
import data from '../Data/data'
class Product extends Component {

    render() {
        const {products} = data;
        // const [cart, setCart] = useState([]);
        return (
            <div className="products">
                <div className="product-banner">
                    <img className="img-pbanner" src="./img/bpselect/bp1.jpg" alt=""/>
                </div>
                <div className="search">
                    <form action>
                        <div className="form-box">
                        <input type="text" className="search-field items" placeholder="search item" />
                        <button className="search-item">Search</button>
                        </div>
                    </form>
                </div>
 
                <div className="container">

                <div className="product-items">
                    {products.map((product,index) =>(
                        <div className="product" key={index}>
                            <div className="product-content">
                                <div className="product-img">
                                    <img className="img-album" src={product.image} alt="kill this love" />
                                </div>
                                <div className="product-btns">
                                <button type="button" className="btn-cart">Add to cart
                                    <span><i className="fas fa-plus" /></span>
                                </button>
                                <button type="button" className="btn-buy">Buy now
                                    <span><i className="fas fa-shopping-cart" /></span>
                                </button>
                                </div>
                            </div>
                            <div className="product-info">
                                <div className="product-info-top">
                                <h2 className="sm-title" />
                                <div className="rating">
                                    <span><i className="fas fa-star" /></span>
                                    <span><i className="fas fa-star" /></span>
                                    <span><i className="fas fa-star" /></span>
                                    <span><i className="fas fa-star" /></span>
                                    <span><i className="fas fa-star" /></span>
                                </div>
                                </div>
                                <a href="#" className="product-name">{product.name}</a>
                                <p className="product-price">{product.price}</p>
                                <p className="product-price">$27</p>
                            </div>
                            <div className="off-info">
                                <h2 className="sm-title">10%</h2>
                            </div>
                        </div>
                    ))}

                </div>
            </div>
        </div>

        );
    }
}

export default Product;