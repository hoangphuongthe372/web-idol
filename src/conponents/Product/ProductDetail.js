import React, { Component } from 'react';

class ProductDetail extends Component {
    render() {
        return (
            <div>
                <div className="card-wrapper">
                    <div className="card">
                        {/* card left */}
                        <div className="product-imgs">
                            <div className="img-display">
                                <div className="img-showcase">
                                    <img src="./img/bpselect/Rosé First Single Album -R-.jpg" alt="shoe image" />
                                    <img src="shoes_images/shoe_2.jpg" />
                                    <img src="shoes_images/shoe_3.jpg" />
                                    <img src="shoes_images/shoe_4.jpg" />
                                </div>
                            </div>
                            {/* <div className="img-select">
                                <div className="img-item">
                                    <a href="#" data-id={1}>
                                    <img src="./img/bpselect/Rosé First Single Album -R-.jpg" alt="shoe image" />
                                    </a>
                                </div>
                                <div className="img-item">
                                    <a href="#" data-id={2}>
                                    <img src="shoes_images/shoe_2.jpg" alt="shoe image" />
                                    </a>
                                </div>
                                <div className="img-item">
                                    <a href="#" data-id={3}>
                                    <img src="shoes_images/shoe_3.jpg" alt="shoe image" />
                                    </a>
                                </div>
                                <div className="img-item">
                                    <a href="#" data-id={4}>
                                    <img src="shoes_images/shoe_4.jpg" alt="shoe image" />
                                    </a>
                                </div>
                            </div> */}
                        </div>
                        {/* card right */}
                        <div className="products-content">
                            <h2 className="product-title">ROSÉ FIRST SINGLE ALBUM -R-</h2>
                            {/* <a href="#" className="product-link">visit nike store</a> */}
                            <div className="product-rating">
                                <i className="fas fa-star" />
                                <i className="fas fa-star" />
                                <i className="fas fa-star" />
                                <i className="fas fa-star" />
                                <i className="fas fa-star-half-alt" />
                            </div>
                            <div className="product-price">
                                <p className="last-price">Old Price: <span>$35</span></p>
                                <p className="new-price">New Price: <span>$30</span></p>
                            </div>
                            <div className="product-detail">
                                <h2>About this item: </h2>
                                <p>* Maximum number of purchases per order: 10</p>
                                <p>* The contents may slightly vary due to the production process</p>
                                <p>* If you purchase pre-order and regular items together, your shipment will be dispatched on the shipping date of the pre-order item you purchase</p>
                                {/* <ul>
                                    <li>Color: <span>Black</span></li>
                                    <li>Available: <span>in stock</span></li>
                                    <li>Category: <span>Shoes</span></li>
                                    <li>Shipping Area: <span>All over the world</span></li>
                                    <li>Shipping Fee: <span>Free</span></li>
                                </ul> */}
                            </div>
                            <div className="purchase-info">
                                <input type="number" min={0} defaultValue={1} />
                                <button type="button" className="btn">
                                    Add to Cart <i className="fas fa-shopping-cart" />
                                </button>
                                <button type="button" className="btn">Compare</button>
                            </div>
                            <div className="social-links">
                                <p>Share At: </p>
                                <a href="#">
                                    <i className="fab fa-facebook-f" />
                                </a>
                                <a href="#">
                                    <i className="fab fa-twitter" />
                                </a>
                                <a href="#">
                                    <i className="fab fa-instagram" />
                                </a>
                                <a href="#">
                                    <i className="fab fa-whatsapp" />
                                </a>
                                <a href="#">
                                    <i className="fab fa-pinterest" />
                                </a>
                            </div>
                        </div>
                    </div>           
                </div>
                
                <div className="products">
                    <div className="product-related">
                        <h2>Related products </h2>
                    </div>
                    <div className="container">
                        <div className="product-items">
                            <div className="product">
                                <div className="product-content">
                                    <div className="product-img">
                                    <img className="img-album" src="./img/bpselect/BLACKPINK 1st FULL ALBUM [THE ALBUM].jpg" alt="kill this love" />
                                    </div>
                                    <div className="product-btns">
                                    <button type="button" className="btn-cart">Add to cart
                                        <span><i className="fas fa-plus" /></span>
                                    </button>
                                    <button type="button" className="btn-buy">Buy now
                                        <span><i className="fas fa-shopping-cart" /></span>
                                    </button>
                                    </div>
                                </div>
                                <div className="product-info">
                                    <div className="product-info-top">
                                    <h2 className="sm-title" />
                                    <div className="rating">
                                        <span><i className="fas fa-star" /></span>
                                        <span><i className="fas fa-star" /></span>
                                        <span><i className="fas fa-star" /></span>
                                        <span><i className="fas fa-star" /></span>
                                        <span><i className="fas fa-star" /></span>
                                    </div>
                                    </div>
                                    <a href="#" className="product-name">THE ALBUM</a>
                                    <p className="product-price">$30</p>
                                    <p className="product-price">$25</p>
                                </div>
                                <div className="off-info">
                                    <h2 className="sm-title">5%</h2>
                                </div>
                            </div>

                            <div className="product">
                                <div className="product-content">
                                    <div className="product-img">
                                    <img className="img-album" src="./img/bpselect/BLACKPINK 1st FULL ALBUM [THE ALBUM].jpg" alt="kill this love" />
                                    </div>
                                    <div className="product-btns">
                                    <button type="button" className="btn-cart">Add to cart
                                        <span><i className="fas fa-plus" /></span>
                                    </button>
                                    <button type="button" className="btn-buy">Buy now
                                        <span><i className="fas fa-shopping-cart" /></span>
                                    </button>
                                    </div>
                                </div>
                                <div className="product-info">
                                    <div className="product-info-top">
                                    <h2 className="sm-title" />
                                    <div className="rating">
                                        <span><i className="fas fa-star" /></span>
                                        <span><i className="fas fa-star" /></span>
                                        <span><i className="fas fa-star" /></span>
                                        <span><i className="fas fa-star" /></span>
                                        <span><i className="fas fa-star" /></span>
                                    </div>
                                    </div>
                                    <a href="#" className="product-name">THE ALBUM</a>
                                    <p className="product-price">$30</p>
                                    <p className="product-price">$25</p>
                                </div>
                                <div className="off-info">
                                    <h2 className="sm-title">5%</h2>
                                </div>
                            </div>

                            <div className="product">
                                <div className="product-content">
                                    <div className="product-img">
                                    <img className="img-album" src="./img/bpselect/BLACKPINK 1st VINYL LP [THE ALBUM] -LIMITED EDITION-.jpg" alt="kill this love" />
                                    </div>
                                    <div className="product-btns">
                                    <button type="button" className="btn-cart">Add to cart
                                        <span><i className="fas fa-plus" /></span>
                                    </button>
                                    <button type="button" className="btn-buy">Buy now
                                        <span><i className="fas fa-shopping-cart" /></span>
                                    </button>
                                    </div>
                                </div>
                                <div className="product-info">
                                    <div className="product-info-top">
                                    <h2 className="sm-title" />
                                    <div className="rating">
                                        <span><i className="fas fa-star" /></span>
                                        <span><i className="fas fa-star" /></span>
                                        <span><i className="fas fa-star" /></span>
                                        <span><i className="fas fa-star" /></span>
                                        <span><i className="fas fa-star" /></span>
                                    </div>
                                    </div>
                                    <a href="#" className="product-name">LP THE ALBUM - LIMITED EDITION -</a>
                                    <p className="product-price">$35</p>
                                    <p className="product-price">$30</p>
                                </div>
                                <div className="off-info">
                                    <h2 className="sm-title">10%</h2>
                                </div>
                            </div>

                            <div className="product">
                            <div className="product-content">
                                <div className="product-img">
                                <img className="img-album" src="./img/bpselect/BLACKPINK SPECIAL EDITION [How You Like That].jpg" alt="kill this love" />
                                </div>
                                <div className="product-btns">
                                <button type="button" className="btn-cart">Add to cart
                                    <span><i className="fas fa-plus" /></span>
                                </button>
                                <button type="button" className="btn-buy">Buy now
                                    <span><i className="fas fa-shopping-cart" /></span>
                                </button>
                                </div>
                            </div>
                            <div className="product-info">
                                <div className="product-info-top">
                                <h2 className="sm-title" />
                                <div className="rating">
                                    <span><i className="fas fa-star" /></span>
                                    <span><i className="fas fa-star" /></span>
                                    <span><i className="fas fa-star" /></span>
                                    <span><i className="fas fa-star" /></span>
                                    <span><i className="fas fa-star" /></span>
                                </div>
                                </div>
                                <a href="#" className="product-name">SPECIAL EDITION How You Like That</a>
                                <p className="product-price">$30</p>
                                <p className="product-price">$27</p>
                            </div>
                            <div className="off-info">
                                <h2 className="sm-title">5%</h2>
                            </div>
                        </div>

                        </div>
                    </div>
                </div>
                
                
            </div>
            

        );
    }
}

export default ProductDetail;