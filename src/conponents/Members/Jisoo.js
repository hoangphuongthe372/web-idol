
import React, { Component } from 'react'

class Jisoo extends Component{
    render(){
        return (

                <div id="content">
                    {/* introduce */}
                    <div id="introduce" className="content-section">
                        <div className="section-heading">JISOO</div>
                        <p className="section-sub-heading">Singer</p>
                        <p className="about-text">
                        Kim Jisoo (Korean: 김지수; born January 3, 1995), better known mononymously as Jisoo, is a South Korean singer and actress. She made her debut in August 2016 as a member of the girl group Blackpink under YG Entertainment.
                        <br></br>
                        <br></br>Besides being a singer and K-pop idol, Jisoo also participates in acting and hosting. She played many commercials, starred in many films as a guest before receiving her first leading role in her acting career in the TV series Snowdrop, scheduled to air in early 2021.
                        </p>
                        <p className="section-sub-heading">Kim Jisoo</p>
                        <div class="row members-list">

                            <div class="col col-2 text-center">
                                <img src="./img/members/jisoo/ji7.jpg" alt="" className="all-members-ava" />
                            </div>

                            <div class="col col-2 text-center">                               
                                <img src="./img/members/jisoo/ji19.jpg" alt="" className="all-members-ava" />
                            </div>
        
        
                            <div class="col col-1 text-center">
                                <img src="./img/members/jisoo/ji9.jpg" alt="" className="all-members-ava" />
                            </div>
        
                            <div className="clear"></div>
                        </div>
                    </div>
                    

                    {/* global */}
                    <div id="album" className="background-jisoo-dior">
                        <div className="content-section">
                            
                                <div className="section-heading">Dior</div>
                                <p className="section-sub-heading">Global Abassador</p>
                                <p className="about-text">
                                    
                                In December 2019, Jisoo became a local ambassador for Dior's cosmetics brand "Dior Beauty". The next summer, Jisoo was recruited to be Dior's muse and modelled for Dior's Fall/Winter 2020 collection. In September 2020, Jisoo covered the 155th edition 2020 of Dazed Korea, where she discussed her work with Dior. She was also appointed as the first main model for Cartier's digital project in the return of "Pasha de Cartier" for Korean MZ generations. In December 2020, Jisoo was photographed with the Lady Dior and D'Lite bags in Dior's Cruise 2020–2021 Collection.
                                    <br></br> 
                                    <br></br> 

                                    In January 2021, Dior Beauty's "Dior Forever Skin Glow Cushion", endorsed by Jisoo, was released exclusively in Korea. She also sighted in promotional Spring/Summer Collection for wearing Dior's Caro bag. The next March, she began expanding her long-partnership with Dior for both Fashion and Beauty area, after revealed by the French Houses as their new Global Ambassador, entitled her name alongside actresses Natalie Portman and Cara Delevingne.
                                </p>
                            <div class="row album-list">
                                <div class="col album-item">
                                    <img src="../img/members/jisoo/ji12.jpg" alt="Instagram" className="album-img" />
                                </div>
        
                                <div class="col album-item">
                                    <img src="../img/members/jisoo/ji13.jpg" alt="Instagram" className="album-img" />
                                </div>
                                
                                <div className="clear"></div>
                            </div>
                            <h3 className="section-sub-heading album-heading">Jisoo with Dior</h3>
                        </div>
                    </div>

                    {/* influence */}
                    <div id="influence" className="content-section">
                        <div className="section-heading">INFLUENCE</div>
                        <p className="section-sub-heading">Social Network</p>
                        <p className="about-text">
                        In Gallup Korea's annual music poll for 2018, Jisoo was ranked the tenth most trending idol in South Korea, receiving 4.8% of the votes. In April 2019, she was ranked the tenth most followed K-pop idol on Instagram, with 12.8 million followers. In 2019, Jisoo was also ranked as the sixth most popular female K-pop idol in a survey of soldiers doing mandatory military service in South Korea. In 2019 she was chosen as a part of the BoF 500, a "definitive professional index" of people shaping the $2.4 trillion fashion industry.
                        </p>
                        <div className="influence-list">
                            <div className="influence-item">
                                <img src="../img/members/jisoo/ji25.jpg" alt="Instagram" className="influence-img" />
                                <div className="influence-content">
                                    <h3 className="influence-heading">sooyaaa__</h3>
                                    <p className="influence-icon">Instagram</p>
                                    <p className="influence-desc">42M followers</p>
                                    <a href="/#" className="influence-redirect">Visit pages</a>
                                </div>
                            </div>
        
                            <div className="influence-item">
                                <img src="../img/members/jisoo/ji26.jpg" alt="Instagram" className="influence-img" />
                                <div className="influence-content">
                                    <h3 className="influence-heading">BLACKPINK</h3>
                                    <p className="influence-icon">Youtube</p>
                                    <p className="influence-desc">62M followers</p>
                                    <a href="/#" className="influence-redirect">Visit pages</a>
                                </div>
                            </div>
        
                            <div className="influence-item">
                                <img src="../img/members/jisoo/ji24.jpg" alt="Instagram" className="influence-img" />
                                <div className="influence-content">
                                    <h3 className="influence-heading">BLACKPINK</h3>
                                    <p className="influence-icon">Vlive</p>
                                    <p className="influence-desc">12.5M followers</p>
                                    <a href="/#" className="influence-redirect">Visit pages</a>
                                </div>
                            </div>
                            <div className="clear"></div>
                        </div>
                    </div>

                </div>
    
        );
    }
}

export default Jisoo;
