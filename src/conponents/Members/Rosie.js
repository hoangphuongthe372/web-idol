
import React, { Component } from 'react'

class Rosie extends Component{
    render(){
        return (

                <div id="content">
                    {/* introduce */}
                    <div id="introduce" className="content-section">
                        <div className="section-heading">ROSÉ</div>
                        <p className="section-sub-heading">Singer</p>
                        <p className="about-text">
                            Roseanne Park (born 11 February 1997), better known by the mononym Rosé (Korean: 로제), is a Korean-New Zealand singer based in South Korea.[3][1] Born in New Zealand and raised in Australia, Rosé signed with South Korean label YG Entertainment following an audition in 2012, training there for four years. She made her debut as a vocalist in the girl group Blackpink in August 2016 and made her solo debut with her single album R in March 2021.
                        </p>
                        <p className="section-sub-heading">Park Chaeyoung</p>
                        <div class="row members-list">
                            <div class="col col-2 text-center">                               
                                <img src="./img/members/rosie/r2.jpg" alt="" className="all-members-ava" />
                            </div>
        
                            <div class="col col-2 text-center">
                                <img src="./img/members/rosie/r3.jpg" alt="" className="all-members-ava" />
                            </div>
        
                            <div class="col col-1 text-center">
                                <img src="./img/members/rosie/r1.jpg" alt="" className="all-members-ava" />
                            </div>
        
                            <div className="clear"></div>
                        </div>
                    </div>
                    {/* album section */}
                    <div id="album" className="background-rosie">
                        <div className="content-section">
                            
                                <div className="section-heading">-R-</div>
                                <p className="section-sub-heading">Album</p>
                                <p className="about-text">
                                On 2 June 2020, it was announced that Rosé would debut solo following the release of Blackpink's first Korean language full length album. On 30 December 2020, in an interview with South Korean media outlet Osen, Rosé revealed that filming for her debut music video would begin in mid-January 2021. On 26 January 2021, a promotional teaser of Rosé's solo debut was released, revealing that a preview of her solo debut would be revealed through Blackpink Livestream Concert: The Show on 31 January 2021. Shortly after the performance, Rosé’s solo, titled "Gone" gained positive feedback from Korean news website My Daily.
                                    <br></br> 
                                    <br></br> 
                                    Rosé's debut single album, entitled R, was released on 12 March 2021. Upon release, with 41.6 million views in 24 hours on her lead single "On the Ground", she currently holds the title as the most viewed South Korean music video of a soloist in 24 hours after breaking the almost 8-year record of former labelmate Psy's "Gentleman". "On the Ground" peaked at number 70 on the Billboard Hot 100, becoming the highest-charting song by a Korean female soloist in the US. The song also debuted and peaked at number 1 on both the Global 200 and Global Excl. U.S. charts, the first song by a Korean solo artist to do so in the charts' histories.R also set the record for the highest first-week sales by a Korean female soloist with 448,089 copies sold. On 24 March, Rosé received her first-ever music show win as a soloist with her single "On the Ground" through South Korean cable music program, Show Champion
                                </p>
                            <p className="section-sub-heading">Single Album</p>
                            <div class="row album-list">
                                <div class="col album-item">
                                    <img src="./img/content/otg.jpg" alt="Instagram" className="album-img" />
                                    <div className="album-content">
                                        <h3 className="album-heading">On The Ground</h3>
                                        {/* <p className="album-desc">30$</p> */}
                                        {/* <a href="/#" className="album-redirect">View Albums</a> */}
                                    </div>
                                </div>
        
                                <div class="col album-item">
                                    <img src="./img/content/gone.jpg" alt="Instagram" className="album-img" />
                                    <div className="album-content">
                                        <h3 className="album-heading">Gone</h3>
                                        {/* <p className="album-desc">30$</p> */}
                                        {/* <a href="/#" className="album-redirect">View Albums</a> */}
                                    </div>
                                </div>
                                <div className="clear"></div>
                            </div>
        
                        </div>
                    </div>
        
                    {/* influence */}
                    <div id="influence" className="content-section">
                        <div className="section-heading">INFLUENCE</div>
                        <p className="section-sub-heading">Social Network</p>
                        <p className="about-text">
                            Through a radio interview, the singer cited her labelmate senior Big Bang's Taeyang as a role model towards her musical career. As a musician, Rosé also revealed she considers American singer Tori Kelly as an inspiration towards her musical style. Rosé's voice has received acknowledgement in the K-pop industry for its distinct vocal timbre, following her debut as a member of Blackpink. Following Rosé's performance on an episode of Fantastic Duo 2, South Korean singer Gummy, whom Rosé cited as a musical role model, stated that "[Rosé's] voice is so unique, it's the [type of] voice young people love".
                        </p>
                        <div className="influence-list">
                            <div className="influence-item">
                                <img src="./img/influence/inf-chaeng.jpg" alt="Instagram" className="influence-img" />
                                <div className="influence-content">
                                    <h3 className="influence-heading">roses_are_rosie</h3>
                                    <p className="influence-icon">Instagram</p>
                                    <p className="influence-desc">38.6M followers</p>
                                    <a href="/#" className="influence-redirect">Visit pages</a>
                                </div>
                            </div>
        
                            <div className="influence-item">
                                <img src="./img/influence/inf-chaeng1.jpg" alt="Instagram" className="influence-img" />
                                <div className="influence-content">
                                    <h3 className="influence-heading">ROSÉ</h3>
                                    <p className="influence-icon">Youtube</p>
                                    <p className="influence-desc">2.05M followers</p>
                                    <a href="/#" className="influence-redirect">Visit pages</a>
                                </div>
                            </div>
        
                            <div className="influence-item">
                                <img src="./img/influence/inf-chaeng2.jpg" alt="Instagram" className="influence-img" />
                                <div className="influence-content">
                                    <h3 className="influence-heading">roses_are_rosie</h3>
                                    <p className="influence-icon">Tiktok</p>
                                    <p className="influence-desc">10.6M followers</p>
                                    <a href="/#" className="influence-redirect">Visit pages</a>
                                </div>
                            </div>
                            <div className="clear"></div>
                        </div>
                    </div>

                    {/* album section */}
                    <div id="album" className="background-rosie-ysl">
                        <div className="content-section">
                            
                                <div className="section-heading">Yves Saint Laurent</div>
                                <p className="section-sub-heading">Global Abassador</p>
                                <p className="about-text">
                                Rosé is YSL's first global brand ambassador. Before Rosé, no one had been recognized by YSL as a 'global ambassador' throughout the brand's history. This is not only a surprise for Rosé but also a milestone for the "big" French when there is only one global brand ambassador in 59 years of history. So Rosé is no longer associated with the title of the picturesque "Saint Laurent girl" but has stepped up to a new position, creating a new relationship with the famous fashion house.
                                    <br></br> 
                                    <br></br> 
                                    Blackpink Rosé shared with Elle: "Up to now I still cannot believe I have become the global ambassador for the Saint Laurent brand. I think it is really great. This is an honor for me. Anthony. Vacarello is also very close to me, I learn from him a lot, from creativity, professionalism to humanistic thought, it is great to have the opportunity to work with such people. "
                                </p>
                            <div class="row album-list">
                                <div class="col album-item">
                                    <img src="../img/members/rosie/r5.jpg" alt="Instagram" className="album-img" />
                                </div>
        
                                <div class="col album-item">
                                    <img src="../img/members/rosie/rosie-with-an.jpg" alt="Instagram" className="album-img" />
                                </div>
                                
                                <div className="clear"></div>
                            </div>
                            <h3 className="section-sub-heading album-heading">Rosé and Anthony Vaccarello</h3>
        
                        </div>
                    </div>
                </div>
    
        );
    }
}

export default Rosie;
