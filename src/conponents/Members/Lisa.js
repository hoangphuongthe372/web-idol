
import React, { Component } from 'react'

class Lisa extends Component{
    render(){
        return (

                <div id="content">
                    {/* introduce */}
                    <div id="introduce" className="content-section">
                        <div className="section-heading">LISA</div>
                        <p className="section-sub-heading">Rapper</p>
                        <p className="about-text">
                        Lalisa Manoban (Thai: ลลิษา มโนบาล; born Pranpriya Manoban (Thai: ปราณปรียา มโนบาล); March 27, 1997), better known by the mononym Lisa (Hangul: 리사), is a Thai rapper, singer, and dancer based in South Korea. She is a member of the South Korean girl group Blackpink under YG Entertainment.
                        </p>
                        <p className="section-sub-heading">Lalisa Manoban</p>
                        <div class="row members-list">
                            <div class="col col-2 text-center">                               
                                <img src="./img/members/lisa/l1.jpg" alt="" className="all-members-ava" />
                            </div>
        
                            <div class="col col-2 text-center">
                                <img src="./img/members/lisa/l3.jpg" alt="" className="all-members-ava" />
                            </div>
        
                            <div class="col col-1 text-center">
                                <img src="./img/members/lisa/l2.jpg" alt="" className="all-members-ava" />
                            </div>
        
                            <div className="clear"></div>
                        </div>
                    </div>
                    {/* album section */}
                    {/* <div id="album" className="background-rosie">
                        <div className="content-section">
                            
                                <div className="section-heading">-R-</div>
                                <p className="section-sub-heading">Album</p>
                                <p className="about-text">
                                On 2 June 2020, it was announced that Rosé would debut solo following the release of Blackpink's first Korean language full length album. On 30 December 2020, in an interview with South Korean media outlet Osen, Rosé revealed that filming for her debut music video would begin in mid-January 2021. On 26 January 2021, a promotional teaser of Rosé's solo debut was released, revealing that a preview of her solo debut would be revealed through Blackpink Livestream Concert: The Show on 31 January 2021. Shortly after the performance, Rosé’s solo, titled "Gone" gained positive feedback from Korean news website My Daily.
                                    <br></br> 
                                    <br></br> 
                                    Rosé's debut single album, entitled R, was released on 12 March 2021. Upon release, with 41.6 million views in 24 hours on her lead single "On the Ground", she currently holds the title as the most viewed South Korean music video of a soloist in 24 hours after breaking the almost 8-year record of former labelmate Psy's "Gentleman". "On the Ground" peaked at number 70 on the Billboard Hot 100, becoming the highest-charting song by a Korean female soloist in the US. The song also debuted and peaked at number 1 on both the Global 200 and Global Excl. U.S. charts, the first song by a Korean solo artist to do so in the charts' histories.R also set the record for the highest first-week sales by a Korean female soloist with 448,089 copies sold. On 24 March, Rosé received her first-ever music show win as a soloist with her single "On the Ground" through South Korean cable music program, Show Champion
                                </p>
                            <p className="section-sub-heading">Single Album</p>
                            <div class="row album-list">
                                <div class="col album-item">
                                    <img src="./img/content/otg.jpg" alt="Instagram" className="album-img" />
                                    <div className="album-content">
                                        <h3 className="album-heading">On The Ground</h3>
                                        <p className="album-desc">30$</p>
                                        <a href="/#" className="album-redirect">Buy Albums</a>
                                    </div>
                                </div>
        
                                <div class="col album-item">
                                    <img src="./img/content/gone.jpg" alt="Instagram" className="album-img" />
                                    <div className="album-content">
                                        <h3 className="album-heading">Gone</h3>
                                        <p className="album-desc">30$</p>
                                        <a href="/#" className="album-redirect">Buy Albums</a>
                                    </div>
                                </div>
                                <div className="clear"></div>
                            </div>
        
                        </div>
                    </div> */}

                    {/* global */}
                    <div id="album" className="background-lisa-celine">
                        <div className="content-section">
                            
                                <div className="section-heading">Celine</div>
                                <p className="section-sub-heading">Global Abassador</p>
                                <p className="about-text">
                                    
                                    Lisa officially becomes the Muse of Celine and Hedi Slimane. Celine has never had any Brand Ambassadors, and Lisa is currently Celine's only Muse and even Hedi Slimane.
                                    <br></br> 
                                    <br></br> 
                                    Lisa was the first Asian to be photographed personally by Hedi Slimane. The set of photos that Hedi Slimane took for Lisa appeared on the covers of two magazines, Madame Figaro Japan in January 2020, the 30th anniversary of the Japanese publication and the Vogue Hong Kong special edition Christmas 12/2019.
                                </p>
                            <div class="row album-list">
                                <div class="col album-item">
                                    <img src="../img/members/lisa/l4.jpg" alt="Instagram" className="album-img" />
                                </div>
        
                                <div class="col album-item">
                                    <img src="../img/members/lisa/l5.jpg" alt="Instagram" className="album-img" />
                                </div>
                                
                                <div className="clear"></div>
                            </div>
                            <h3 className="section-sub-heading album-heading">Lisa and Hedi Slimane</h3>
                        </div>
                    </div>

                    {/* influence */}
                    <div id="influence" className="content-section">
                        <div className="section-heading">INFLUENCE</div>
                        <p className="section-sub-heading">Social Network</p>
                        <p className="about-text">
                        In April 2019, Lisa became the most followed K-pop idol on Instagram, with 17.4 million followers at the time. As of April 2021, she is the first and only K-pop idol to amass 50 million followers, as she continues to set engagement and follower count records on the platform.
                        </p>
                        <div className="influence-list">
                            <div className="influence-item">
                                <img src="../img/members/lisa/l6.jpg" alt="Instagram" className="influence-img" />
                                <div className="influence-content">
                                    <h3 className="influence-heading">lalalalisa_m</h3>
                                    <p className="influence-icon">Instagram</p>
                                    <p className="influence-desc">50.3M followers</p>
                                    <a href="/#" className="influence-redirect">Visit pages</a>
                                </div>
                            </div>
        
                            <div className="influence-item">
                                <img src="../img/members/lisa/l7.jpg" alt="Instagram" className="influence-img" />
                                <div className="influence-content">
                                    <h3 className="influence-heading">Lilifilm Official</h3>
                                    <p className="influence-icon">Youtube</p>
                                    <p className="influence-desc">6.58M followers</p>
                                    <a href="/#" className="influence-redirect">Visit pages</a>
                                </div>
                            </div>
        
                            <div className="influence-item">
                                <img src="../img/members/lisa/l8.jpg" alt="Instagram" className="influence-img" />
                                <div className="influence-content">
                                    <h3 className="influence-heading">lalalalisa_m</h3>
                                    <p className="influence-icon">Weibo</p>
                                    <p className="influence-desc">40.7K followers</p>
                                    <a href="/#" className="influence-redirect">Visit pages</a>
                                </div>
                            </div>
                            <div className="clear"></div>
                        </div>
                    </div>

                </div>
    
        );
    }
}

export default Lisa;
