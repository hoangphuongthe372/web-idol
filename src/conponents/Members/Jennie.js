
import React, { Component } from 'react'

class Jennie extends Component{
    render(){
        return (

                <div id="content">
                    {/* introduce */}
                    <div id="introduce" className="content-section">
                        <div className="section-heading">Jennie</div>
                        <p className="section-sub-heading">Singer & rapper</p>
                        <p className="about-text">
                        Jennie Kim (Korean: 김제니; born January 16, 1996), known mononymously as Jennie, is a South Korean singer and rapper. Born and raised in South Korea, Kim studied in New Zealand at the age of eight for five years, before returning to South Korea in 2010. She debuted as a member of the girl group Blackpink under YG Entertainment in August 2016. In November 2018, Jennie made her debut as a solo artist with the single "Solo". The song was a commercial success domestically and internationally, topping both the Gaon Digital Chart and Billboard's World Digital Songs chart.
                        </p>
                        <p className="section-sub-heading">Jennie Kim</p>
                        <div class="row members-list">
                            <div class="col col-2 text-center">                               
                                <img src="./img/members/jennie/je18.jpg" alt="" className="all-members-ava" />
                            </div>
        
                            <div class="col col-2 text-center">
                                <img src="./img/members/jennie/je17.jpg" alt="" className="all-members-ava" />
                            </div>
        
                            <div class="col col-1 text-center">
                                <img src="./img/members/jennie/je3.jpg" alt="" className="all-members-ava" />
                            </div>
        
                            <div className="clear"></div>
                        </div>
                    </div>
                    {/* album section */}
                    <div id="album" className="background-jennie">
                        <div className="content-section">
                            
                                <div className="section-heading">SOLO</div>
                                <p className="section-sub-heading">Album</p>
                                <p className="about-text">
                                "Solo" is the debut solo single by South Korean rapper, singer and Blackpink member Jennie, released on November 12, 2018 through YG and Interscope. The song was written by Teddy Park and produced by him alongside 24. Musically, "Solo" is a dance, pop and hip hop song with EDM sound, with its lyrical content revolving around the theme of independence after a break-up.
                                    <br></br> 
                                    <br></br> 
                                    The song was a commercial success in South Korea, where it debuted and peaked at number one on the Gaon Digital Chart. Moreover, the song attained international success, becoming her first chart-topper on Billboard's World Digital Songs chart. "Solo" debuted in the charts of several countries, including Canada, Japan, Malaysia, New Zealand, Scotland, Singapore and the United Kingdom. The song has been certified platinum twice by the Korea Music Content Association (KMCA).
                                </p>
                            <p className="section-sub-heading">Single Album</p>
                            <div class="row album-list">
                                <div class="col album-item">
                                    <img src="../img/members/jennie/je9.jpg" alt="Instagram" className="album-img" />
                                    <div className="album-content">
                                        <h3 className="album-heading">SOLO</h3>
                                        {/* <p className="album-desc">30$</p> */}
                                        {/* <a href="/#" className="album-redirect">View Album</a> */}
                                    </div>
                                </div>
        
                                <div class="col album-item">
                                    <img src="../img/members/jennie/je8.jpg" alt="Instagram" className="album-img" />
                                    <div className="album-content">
                                        <h3 className="album-heading">solo limited edition</h3>
                                        {/* <p className="album-desc">30$</p> */}
                                        {/* <a href="/#" className="album-redirect">View Album</a> */}
                                    </div>
                                </div>
                                <div className="clear"></div>
                            </div>
        
                        </div>
                    </div>
        
                    {/* influence */}
                    <div id="influence" className="content-section">
                        <div className="section-heading">INFLUENCE</div>
                        <p className="section-sub-heading">Social Network</p>
                        <p className="about-text">
                        On January 16, 2021, Jennie commenced her official YouTube channel, coinciding with her 25th birthday. Her channel quickly gained 1 million subscribers in less than seven hours and became the fastest channel to do so on the platform. Within 24 hours, she accumulating over 1.75 million subscribers, and became the second most subscribed YouTube user within the timeframe, following Brazilian singer Marília Mendonça.
                        </p>
                        <div className="influence-list">
                            <div className="influence-item">
                                <img src="../img/members/jennie/je13.jpg" alt="Instagram" className="influence-img" />
                                <div className="influence-content">
                                    <h3 className="influence-heading">jennierubyjane</h3>
                                    <p className="influence-icon">Instagram</p>
                                    <p className="influence-desc">46.6M followers</p>
                                    <a href="/#" className="influence-redirect">Visit pages</a>
                                </div>
                            </div>
        
                            <div className="influence-item">
                                <img src="../img/members/jennie/je14.jpg" alt="Instagram" className="influence-img" />
                                <div className="influence-content">
                                    <h3 className="influence-heading">Jennierubyjane Official</h3>
                                    <p className="influence-icon">Youtube</p>
                                    <p className="influence-desc">6.1M followers</p>
                                    <a href="/#" className="influence-redirect">Visit pages</a>
                                </div>
                            </div>
        
                            <div className="influence-item">
                                <img src="../img/members/jennie/je12.jpg" alt="Instagram" className="influence-img" />
                                <div className="influence-content">
                                    <h3 className="influence-heading">lesyeuxdenini</h3>
                                    <p className="influence-icon">Instagram</p>
                                    <p className="influence-desc">2.6M followers</p>
                                    <a href="/#" className="influence-redirect">Visit pages</a>
                                </div>
                            </div>
                            <div className="clear"></div>
                        </div>
                    </div>

                    {/* album section */}
                    <div id="album" className="background-jennie-chanel">
                        <div className="content-section">
                            
                                <div className="section-heading">Chanel</div>
                                <p className="section-sub-heading">House Ambassador</p>
                                <p className="about-text">
                                House of Chanel chose Jennie as "House Ambassador" after eyeing the ripple effects she had.[65] She has significant selling power; photos on her social media become articles and the items she shows become trends. The hairpins Jennie wore in a 5-by-5 partition during her performances of her debut single "Solo" instantly became a trend in South Korea and were referred to as "Jennie's hairpins" due to their popularity.
                                    <br></br> 
                                    <br></br> 
                                    On April 15, 2020, it was announced that Jennie collaborated with Gentle Monster, a South Korean luxury eyewear brand. The collaboration was titled Jentle Home and was inspired by Jennie's childhood memories. The collections were designed by herself and features glasses, sunglasses, eyewear chains and others. In May 2020, Gentle Monster and Jennie opened a dollhouse-themed pop up store in Gangnam, Seoul.
                                </p>
                            <div class="row album-list">
                                <div class="col album-item">
                                    <img src="../img/members/jennie/je15.jpg" alt="Instagram" className="album-img" />
                                </div>
        
                                <div class="col album-item">
                                    <img src="../img/members/jennie/je16.jpg" alt="Instagram" className="album-img" />
                                </div>
                                
                                <div className="clear"></div>
                            </div>
                            <h3 className="section-sub-heading album-heading">jennie and Virginie Viard</h3>
        
                        </div>
                    </div>
                </div>
    
        );
    }
}

export default Jennie
