
import React, { useState } from 'react'
import './Slider.css'
import ImgSlider from './ImgSlider'
import i1 from "./slider/bp1.jpg"
import i2 from "./slider/vogue.jpg"
import i3 from "./slider/blackpink1.png"

function Slider(){
        let sliderArr = [<ImgSlider src={i1} />,<ImgSlider src={i3} />,<ImgSlider src={i2} />];
        const [x,setX] = useState(0);
        const goLeft = () =>{
            setX(x +100);
            x === 0 ?  setX(-100 * (sliderArr.length - 1)) : setX(x + 100);

        }

        const goRight = () =>{
            x === -100 * (sliderArr.length - 1) ? setX(0) : setX(x -100);

        }
        return (
            <div id="slider" className="slider">
                {/* <div className="text-content">
                    <h2 className="text-heading">On The Ground</h2>
                    <p className="text-description">Everything I need is on the ground</p>
                </div> */}

                {
                    sliderArr.map((item,index)=>{
                        return(
                            <div key={index} className="slide" style={{transform:`translateX(${x}%)`}}>
                                {item}
                            </div>
                        );
                    })
                }
                    <button id="goLeft" onClick={goLeft}><i className="fas fa-chevron-left"></i></button>
                    <button id="goRight" onClick={goRight}><i className="fas fa-chevron-right"></i></button>
            </div>
        );
}

export default Slider
