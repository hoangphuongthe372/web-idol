const data = {
    products: [
        {
            id: '1',
            name : 'Rosé First Single Album -R-',
            price : '$30',
            salePrice: '$27',
            perPrice: '10%',
            image : './img/bpselect/Rosé First Single Album -R-.jpg'
       },

       {
            id: '2',
            name : 'Rosé First Single Album -R- Kit Album',
            price : '$30',
            salePrice: '$27',
            perPrice: '10%',
            image : './img/bpselect/Rosé First Single Album -R- Kit Album.jpg'
        },

        {
            id: '3',
            name : 'LISA PHOTOBOOK 0327 -LIMITED EDITION',
            price : '$30',
            salePrice: '$27',
            perPrice: '10%',
            image : './img/bpselect/LISA PHOTOBOOK [0327] -LIMITED EDITION-.jpg'
        },

        {
            id: '4',
            name : 'LISA PHOTOBOOK 0327 VOL.2 -SECOND EDITION-',
            price : '$30',
            salePrice: '$27',
            perPrice: '10%',
            image : './img/bpselect/LISA PHOTOBOOK [0327] VOL.2 -SECOND EDITION-.jpg'
        },

        {
            id: '5',
            name : 'THE ALBUM',
            price : '$30',
            salePrice: '$27',
            perPrice: '10%',
            image : './img/bpselect/BLACKPINK 1st FULL ALBUM [THE ALBUM].jpg'
        },

        {
            id: '6',
            name : 'LP THE ALBUM - LIMITED EDITION -',
            price : '$35',
            salePrice: '$30',
            perPrice: '15%',
            image : './img/bpselect/BLACKPINK 1st VINYL LP [THE ALBUM] -LIMITED EDITION-.jpg'
        },


        {
            id: '7',
            name : 'SPECIAL EDITION How You Like That',
            price : '$30',
            salePrice: '$27',
            perPrice: '10%',
            image : './img/bpselect/BLACKPINK SPECIAL EDITION [How You Like That].jpg'
        },

        {
            id: '8',
            name : '2021 SEASON’S GREETINGS -ORIGINAL-',
            price : '$30',
            salePrice: '$27',
            perPrice: '10%',
            image : './img/bpselect/BLACKPINK 2021 SEASON’S GREETINGS -ORIGINAL-.jpg'
        },

        {
            id: '9',
            name : '2021 SEASON’S GREETINGS -KiT VIDEO-',
            price : '$35',
            salePrice: '$30',
            perPrice: '15%',
            image : './img/bpselect/BLACKPINK 2021 SEASON’S GREETINGS -KiT VIDEO-.jpg'
        },



        {
            id: '10',
            name : 'Rosé First Single Album -R- Kit Album',
            price : '$30',
            salePrice: '$27',
            perPrice: '10%',
            image : './img/bpselect/Rosé First Single Album -R- Kit Album.jpg'
        },

        {
            id: '11',
            name : 'MINI ALBUM KILL THIS LOVE',
            price : '$30',
            salePrice: '$27',
            perPrice: '10%',
            image : './img/bpselect/BLACKPINK 2nd MINI ALBUM [KILL THIS LOVE].jpg'
        },

        {
            id: '12',
            name : 'LIGHT STICK Ver.2 LIMITED EDITION',
            price : '$120',
            salePrice: '$100',
            perPrice: '15%',
            image : './img/bpselect/BLACKPINK OFFICIAL LIGHT STICK Ver.2 LIMITED EDITION.jpg'
        },

        {
            id: '13',
            name : 'SOLO PHOTOBOOK',
            price : '$30',
            salePrice: '$27',
            perPrice: '10%',
            image : './img/bpselect/JENNIE [SOLO] PHOTOBOOK.jpg'
        },

        {
            id: '14',
            name : 'SOLO PHOTOBOOK -SPECIAL EDITION-',
            price : '$30',
            salePrice: '$27',
            perPrice: '10%',
            image : './img/bpselect/JENNIE [SOLO] PHOTOBOOK -SPECIAL EDITION-.jpg'
        }, 
    ],
    // influences : [
    //     {
    //         id : '1',
    //         name : 'Boombayah',
    //         image : './img/content/boombayah.jpg'
    //     },

    //     {
    //         id : '2',
    //         name : 'Whistle',
    //         image : './img/content/whistle.jpg'
    //     }
    // ]
}

export default data;
