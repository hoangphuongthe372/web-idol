
import React, { Component } from 'react'
import data from '../Data/data'
import Slider from '../Slider/Slider';
class main extends Component{
    constructor() {
        super();
        this.state={
            showTitle: true
        }
    }
    operation(){
        this.setState({
            showTitle :false
        })
    }
    render(){
        return (
            <div>

                <Slider/>

                
                <div id="content">
                    {/* introduce */}
                    <div id="introduce" className="content-section">
                        <div className="section-heading">BLACKPINK</div>
                        <p className="section-sub-heading">BLΛƆKPIИK</p>
                        <p className="about-text">
                        &nbsp;&nbsp;&nbsp;&nbsp;Blackpink (Korean: 블랙핑크; commonly stylized as <b>BLACKPINK</b> or <b>BLΛƆKPIИK</b>) is a South Korean girl group formed by <i>YG Entertainment</i>, consisting of members Jisoo, Jennie, Rosé, and Lisa. The group debuted in August 2016 with their single album Square One, which featured "Whistle" and "Boombayah", their first number-one entries on South Korea's Gaon Digital Chart and the Billboard World Digital Song Sales chart, respectively.
                        </p>
                        <p className="section-sub-heading">Members of BLACKPINK</p>
                        <div class="row members-list">
                            <div class="col col-2 text-center">
                                <p className="members-name">JISOO</p>
                                <a href="/jisoo">
                                    <img src="./img/content/jisoo.jpg" alt="" className="members-ava" />
                                </a>
                                
                            </div>
        
                            <div class="col col-2 text-center">
                                <p className="members-name">JENNIE</p>
                                <a href="/jennie">
                                    <img src="./img/content/jennie.jpg" alt="" className="members-ava" />   
                                </a>                                
                            </div>
        
                            <div class="col col-2 text-center">
                                <p className="members-name">ROSÉ</p>
                                <a href="/rosie">
                                    <img src="./img/content/chaeng.jpg" alt="" className="members-ava" /> 
                                </a> 
                                
                            </div>
        
                            <div class="col col-2 text-center">
                                <p className="members-name">LISA</p>
                                <a href="/lisa">
                                    <img src="./img/content/lisa.jpg" alt="" className="members-ava" /> 
                                </a> 
                                
                            </div>
                            <div className="clear"></div>
                        </div>
                    </div>
                    {/* album section */}
                    <div id="album" className="background-content">
                        <div className="content-section">
                            
                                <div className="section-heading">The Album</div>
                                <p className="section-sub-heading">Album</p>
                                <p className="about-text">
                                The Album (stylized in all caps) is the first Korean studio album (second overall) by South Korean girl group Blackpink, released on October 2, 2020, by YG Entertainment and Interscope. It is the group's first full-length work since their debut in 2016. For the album, Blackpink recorded over ten new songs and worked with a variety of producers, including Teddy, Tommy Brown, R. Tee, Steven Franks and 24. Eight songs made the final track list, including two collaborations: "Ice Cream" with Selena Gomez, and "Bet You Wanna" featuring Cardi B. The album explores the group's mature side through the themes of love and the complexities of growing up. Musically, The Album utilizes pop, R&B, hip hop, EDM and trap elements.
                                </p>
                            {/* <p className="section-sub-heading">Single Album</p> */}
                            <div class="row album-list">
                                <div class="col album-item">
                                    <img src="./img/content/Square_One.jpg" alt="Instagram" className="album-img" />
                                    <div className="album-content">
                                        <h3 className="album-heading">Square One</h3>
                                        {/* <a href="/#" className="album-redirect">Buy Albums</a> */}
                                    </div>
                                </div>
        
                                <div class="col album-item">
                                    <img src="./img/content/square-two.jpg" alt="Instagram" className="album-img" />
                                    <div className="album-content">
                                        <h3 className="album-heading">Square Two</h3>
                                        {/* <a href="/#" className="album-redirect">Buy Albums</a> */}
                                    </div>
                                </div>

                                <div class="col album-item">
                                    <img src="./img/content/killthislove.jpg" alt="Instagram" className="album-img" />
                                    <div className="album-content">
                                        <h3 className="album-heading">Kill This Love</h3>
                                        {/* <a href="/#" className="album-redirect">Buy Albums</a> */}
                                    </div>
                                </div>
        
                                <div class="col album-item">
                                    <img src="./img/content/thealbum.jpg" alt="Instagram" className="album-img" />
                                    <div className="album-content">
                                        <h3 className="album-heading">The Album</h3>
                                        {/* <a href="/#" className="album-redirect">Buy Albums</a> */}
                                    </div>
                                </div>
                                <div className="clear"></div>
                            </div>
        
                        </div>
                    </div>
        
                    {/* title */}
                    <div id="influence" className="content-section">
                        {/* Square One */}
                        <div className="section-heading">TITLE SONGS</div>
                        <p className="section-sub-heading">Square One</p>
                        <p className="about-text">
                        "Boombayah" (Korean: 붐바야; RR: Bumbaya) and "Whistle" (Hangul: 휘파람; RR: Hwiparam) was released in the group's digital debut single album titled Square One, on August 8, 2016 by YG Entertainment. "Boombayah" peaked at number 7 in South Korea and topped the Billboard World Digital Song Sales chart in the first week of sales. On October 13, 2020, "Boombayah" became the first K-pop debut music video to surpass 1 billion views on YouTube. 
                        </p>
                        <div className="influence-list">

                            <div class="col album-item">
                                    <img src="./img/content/boombayah.jpg" alt="Instagram" className="album-img" />
                                    <div className="album-content">
                                        <h3 className="album-heading">Boombayah</h3>
                                    </div>
                                </div>
        
                                <div class="col album-item">
                                    <img src="./img/content/whistle.jpg" alt="Instagram" className="album-img" />
                                    <div className="album-content">
                                        <h3 className="album-heading">Whistle</h3>
                                    </div>
                                </div>

                            <div className="clear"></div>
                        </div>
                        
                        {/* Square two */}
                        <div className="section-heading"></div>
                        <p className="section-sub-heading">Square Two</p>
                        <p className="about-text">
                        "Playing with Fire" (Korean: 불장난; RR: Buljangnan) and "Stay" were released on November 1, 2016, as the group's second digital single album titled Square Two, through YG Entertainment. The song was written by Teddy Park and composed by Park and R.Tee.
                        </p>
                        <div className="influence-list">
                        <div class="row album-list">
                                <div class="col album-item">
                                    <img src="./img/content/pwf.jpg" alt="Instagram" className="album-img" />
                                    <div className="album-content">
                                        <h3 className="album-heading">Play With Fire </h3>
                                    </div>
                                </div>
        
                                <div class="col album-item">
                                    <img src="./img/content/stay.jpg" alt="Instagram" className="album-img" />
                                    <div className="album-content">
                                        <h3 className="album-heading">Stay</h3>
                                    </div>
                                </div>
                            </div>

                            <div className="clear"></div>
                        </div>                         
                        
                        
                                        {/* Square up */}

                                        <div className="section-heading"></div>
                                    <p className="section-sub-heading">Square Up</p>
                                    <p className="about-text">
                                    Square Up is the first Korean extended play (second overall) by South Korean girl group Blackpink. It was released on June 15, 2018 by YG Entertainment. It is available in two versions and contains four tracks, with "Ddu-Du Ddu-Du" released as the lead single. Upon its release, Square Up debuted at the top spot of the Gaon Albums Chart and went on to sell almost 179,000 copies on its first fifteen days of release in South Korea. The EP also debuted at number 40 on the US Billboard 200, becoming Blackpink's third highest selling album in a Western market as well as the third highest-charting to date by an all-female K-pop group.
                                    </p>
                                    <div className="influence-list">
                                    <div class="row album-list">
                                            <div class="col album-item">
                                                <img src="./img/content/d4.jpg" alt="Instagram" className="album-img" />
                                                <div className="album-content">
                                                    <h3 className="album-heading">Ddu-Du Ddu-Du</h3>
                                                </div>
                                            </div>
                    
                                            <div class="col album-item">
                                                <img src="./img/content/fy.png" alt="Instagram" className="album-img" />
                                                <div className="album-content">
                                                    <h3 className="album-heading">Forever Young</h3>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="clear"></div>
                                    </div>

                                    {/* kill this love */}
                                    <div className="section-heading"></div>
                                    <p className="section-sub-heading">Kill This Love</p>
                                    <p className="about-text">
                                    Kill This Love  is released on April 5, 2019, by YG Entertainment and Interscope Records. It is their first Korean material since the release of Square Up in June 2018, and their debut release with Interscope Records. The title track was released as the lead single. The single peaked at number two in South Korea and became the group's first top-50 hit in the United States and the United Kingdom.
                                    </p>
                                    <div className="influence-list">
                                    <div class="row album-list">
                                            <div class="col album-item">
                                                <img src="./img/content/ktl.jpg" alt="Instagram" className="album-img" />
                                                <div className="album-content">
                                                    <h3 className="album-heading">Kill This Love</h3>
                                                </div>
                                            </div>
                    
                                            <div class="col album-item">
                                                <img src="./img/content/dnwtd.png" alt="Instagram" className="album-img" />
                                                <div className="album-content">
                                                    <h3 className="album-heading">Don't Know What to Do</h3>
                                                </div>
                                            </div>

                                            <div class="col album-item">
                                                <img src="./img/content/kit.jfif" alt="Instagram" className="album-img" />
                                                <div className="album-content">
                                                    <h3 className="album-heading">Kick It</h3>
                                                </div>
                                            </div>
                    
                                            <div class="col album-item">
                                                <img src="./img/content/hn1.jpg" alt="Instagram" className="album-img" />
                                                <div className="album-content">
                                                    <h3 className="album-heading">Hope Note</h3>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="clear"></div>
                                    </div>

                                    {/* The album */}
                                    <div className="section-heading"></div>
                                    <p className="section-sub-heading">The Album</p>
                                    <p className="about-text">
                                    The Album is the first Korean studio album (second overall) by South Korean girl group Blackpink, released on October 2, 2020, by YG Entertainment and Interscope. It is the group's first full-length work since their debut in 2016.[4] For the album, Blackpink recorded over ten new songs and worked with a variety of producers, including Teddy, Tommy Brown, R. Tee, Steven Franks and 24. Eight songs made the final track list, including two collaborations: "Ice Cream" with Selena Gomez, and "Bet You Wanna" featuring Cardi B. The album explores the group's mature side through the themes of love and the complexities of growing up. Musically, The Album utilizes pop, R&B, hip hop, EDM and trap elements.
                                    </p>
                                    <div className="influence-list">
                                    <div class="row album-list">
                                            <div class="col album-item">
                                                <img src="./img/content/hylt.jpg" alt="Instagram" className="album-img" />
                                                <div className="album-content">
                                                    <h3 className="album-heading">How You Like That</h3>
                                                </div>
                                            </div>
                    
                                            <div class="col album-item">
                                                <img src="./img/content/ic.jpg" alt="Instagram" className="album-img" />
                                                <div className="album-content">
                                                    <h3 className="album-heading">Ice Cream</h3>
                                                </div>
                                            </div>

                                            <div class="col album-item">
                                                <img src="./img/content/lsg.jpg" alt="Instagram" className="album-img" />
                                                <div className="album-content">
                                                    <h3 className="album-heading">Lovesick Girls</h3>
                                                </div>
                                            </div>
                    
                                            <div class="col album-item">
                                                <img src="./img/content/ps.png" alt="Instagram" className="album-img" />
                                                <div className="album-content">
                                                    <h3 className="album-heading">Pretty Savage</h3>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="clear"></div>
                                </div>

                        {/* {
                            this.state.showTitle ? 
                                <div>
                                : <button onClick={ ()=> this.operation() }> Show more</button>
                            } */}


                    </div>
                
                                     

                    {/* album section */}
                    <div id="album" className="background-fashion">
                        <div className="content-section">
                            
                                <div className="section-heading">Fashion Branchs</div>
                                <p className="section-sub-heading">Ambassadors and Muses</p>
                                <p className="about-text branch-text">
                                To claim that Blackpink is a wildly popular K-pop group is an understatement. The four members – Jennie, Rosé, Jisoo and Lisa – have become such a global powerhouse that their fame far transcends just music.

<br></br><br></br>Major French fashion houses have all courted Blackpink. Chanel, Dior, Saint Laurent and Celine are lucky enough to have each recruited one of the girls to become a brand ambassador.
<br></br>
<br></br>Note the word “lucky”. Jennie, Rosé, Jisoo and Lisa are probably the foremost symbols of luxury and style in K-pop. Anything they choose to wear can cause huge ripples in their sea of fans.
                                </p>
       
                        </div>
                    </div>
                </div> 
            </div>
        );
    }
}

export default main;
