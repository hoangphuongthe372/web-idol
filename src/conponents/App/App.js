import './App.css';
import React, { Component} from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';

import RouterURL from '../RouterURL/RouterURL';


// import './public/css/style.css'
// import './public/css/reponsive.css'

class App extends Component{
    render(){
        return (
        
            <div className="App grid">
                <Header />

                <RouterURL/>

                <Footer />
            </div>
        );
    }
}


export default App;
