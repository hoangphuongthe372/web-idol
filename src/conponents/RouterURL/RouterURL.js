import React, { Component } from 'react';
import {BrowserRouter as Router, Route } from "react-router-dom";
import contact from '../Contact/Contact';
import main from '../Home/main';
import Jennie from '../Members/Jennie';
import Jisoo from '../Members/Jisoo';
import Lisa from '../Members/Lisa';
import Rosie from '../Members/Rosie';
import Product from '../Product/Product';
import ProductDetail from '../Product/ProductDetail';

class RouterURL extends Component {
    render() {
        return (
            <Router>
                <div>
                    <Route exact path="/" component={main} />
                    <Route path="/contact" component={contact} />
                    <Route path="/blackpink-select" component={Product} />
                    <Route path="/product-detail" component={ProductDetail} />
                    <Route path="/rosie" component={Rosie} />
                    <Route path="/lisa" component={Lisa} />
                    <Route path="/jennie" component={Jennie} />
                    <Route path="/jisoo" component={Jisoo} />
                </div>
            </Router>
        );
    }
}

export default RouterURL;