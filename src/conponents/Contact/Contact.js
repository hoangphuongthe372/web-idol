import React, { Component } from 'react';
import './Contact.css'

class contact extends Component{
    render(){
        return (
            <div id="content">
                <div id="contact" className="content-section">
                    <div className="section-heading">CONTACT</div>
                    <p className="section-sub-heading">Contact us</p>

                    <div className="row contact-content">
                        <div className="col col-2 s-col-1 contact-info">
                            <p><i className="fas fa-map-marker-alt"></i>Korea</p>
                            <p><i className="fas fa-mobile-alt"></i>Phone: 02.3453.3412~3</p>
                            <p><i className="fas fa-envelope"></i>Email: master@ygeshop.com</p>
                        </div>
                        <div className="col col-2 s-col-1 contact-form">
                            <form action>
                                <div className="row">
                                    <div className="col col-2 s-col-1">
                                    <input type="text" name placeholder="Name" required id className="form-control" />
                                    </div>
                                    <div className="col col-2 s-col-1">
                                    <input type="email" name placeholder="Email" required id className="form-control" />
                                    </div>
                                </div>
                                <div className="row mt-8">
                                    <div className="col col-1">
                                    <input type="text" name placeholder="Message" required id className="form-control" />
                                    </div>
                                </div>
                                <input className="contact-submit mt-16" type="submit" defaultValue="SEND" />
                            </form>
                        </div>
                    </div>
                </div>

                <div classNam="map-section">
                    <img className="map-img" src="./img/content/map.jpg" alt="" />
                </div>
                <div className="clear"></div>
            </div>

            
        )
    }
}

export default contact;
