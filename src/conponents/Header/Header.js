
import React, { Component } from 'react';

class header extends Component{

    render(){

        return (
            <div id="header" class="wrapper">           
                <ul id="nav">
                    <li><a href="/#">Home</a></li>
                    <li><a href="/blackpink-select">Blackpink Select</a></li>
                    <li><a href="/contact">Contact</a></li>
                    <li>
                        <a href="/">
                            Members
                            <i className="nav-arrow-down ti-angle-down"></i>
                        </a>
                        <ul className="subnav">
                            <li><a href="/jisoo">JISOO</a></li>
                            <li><a href="/jennie">JENNIE</a></li>
                            <li><a href="/rosie">ROSÉ</a></li>
                            <li><a href="/lisa">LISA</a></li>
                        </ul>
                    </li>
                </ul>
                <label for="nav-mobile-input" className="nav__bars-button">
                    <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="bars" className="svg-inline--fa fa-bars fa-w-14" role="img" viewBox="0 0 448 512"><path fill="currentColor" d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"/></svg>
                </label>
                <input type="checkbox" className="nav__input" id="nav-mobile-input" />
                <label for="nav-mobile-input" className="nav__overlay"></label>

                <nav className="nav__moblie">
                    <label for="nav-mobile-input" className="nav__mobile-close">
                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" className="svg-inline--fa fa-times fa-w-11" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg>
                    </label>
                    
                    <ul className="nav__mobile-list">
                        <li>
                            <a href="/#" className="nav__mobile-link">HOME</a>
                        </li>
                        <li><a href="/jisoo" className="nav__mobile-link">JISOO</a></li>
                        <li><a href="/jennie" className="nav__mobile-link">JENNIE</a></li>
                        <li><a href="/rosie" className="nav__mobile-link">ROSÉ</a></li>
                        <li><a href="/lisa" className="nav__mobile-link">LISA</a></li>
                    </ul>
                </nav>

                <div className="mobile-menu-btn">
                    <i className="menu-icon fas fa-bars"></i>
                    
                </div>
            </div> 
        
        )
    }
    
}


export default header;
